
import net.bytebuddy.implementation.bind.annotation.IgnoreForBinding;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NewTest extends BaseForTests {


    @Test
    void ajoutenseignant()  {
        login("Turing", "turing");


        click(driver.findElement(By.linkText("Teachers List")));
        //Recuperer la partie  de la liste des enseignats

        String liste1=driver.findElement(By.xpath("//table/tbody")).getText();
        //recupere la table
        click(driver.findElement(By.linkText("Add Teachers")));
        assertTrue(driver.getTitle().contains("Error"));


        //click(driver.findElement(By.linkText("Go back")));
        //String liste2=driver.findElement(By.xpath("//table/tbody")).getText();
        //assertEquals(liste1,liste2);
        //Mettre en place le lien de retour sur la page teacherList

    }

    @Test
    void managerajoutenseignant () throws IOException {
        login("Chef", "mdp");
        click(driver.findElement(By.linkText("Teachers List")));
        click(driver.findElement(By.linkText("Add Teachers")));
        WebElement firstname = driver.findElement(By.id("firstName"));
        WebElement lastname = driver.findElement(By.id("lastName"));
        WebElement submit = driver.findElement(By.cssSelector("[type=submit]"));
        write(firstname, "TeacherfirstnameforTest");
        write(lastname, "TeacherLastNameForTest");
        click(submit);
        assertTrue(driver.getTitle().contains("Teacher List"));

        assertTrue(driver.findElement(By.tagName("body")).getText().contains("TeacherLastNameForTest"));
        screenshot("screenshot_Test");
    }



}
